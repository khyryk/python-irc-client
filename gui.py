from tray import TrayIcon
from PySide import QtGui, QtCore, QtNetwork

"""
Proposed internal tab naming convention:
{"server", "server:#chan", "server:private"}

This solves the problem of possible name collision as there can be duplicate channels across different servers,
still allowing me to store the names in a set or dict.

Also, I can immediately identify the type of the tab by its internal name.

Alternative:
Extend the tab class?
{"server": {"#chan",...}, "server2": {"#chan",...}} -- but how to go from chan back to server?
[server, #chan, #chan2, ...]
"""


class IrcGui(QtGui.QMainWindow):
    def __init__(self):
        super(IrcGui, self).__init__()
        self.tabs = QtGui.QTabWidget()
        self.tabID = dict()  # "internal tab name" -> tab reference
        self.sockets = dict()  # tab reference -> socket
        self.time = QtCore.QTime()
        self.initUI()
        self.connectPrompt()

    def initUI(self):
        self.time.start()
        self.menubar = self.menuBar()
        fileMenu = self.menubar.addMenu("&File")
        fileMenu.addAction("connect").triggered.connect(self.connectPrompt)

        background = QtGui.QPixmap("Space.jpg")
        palette = QtGui.QPalette()
        palette.setBrush(QtGui.QPalette.Background, background)
        self.setPalette(palette)

        # tab functionality
        self.tabs.setDocumentMode(True)  # necessary for tabs to remain transparent
        self.tabs.currentChanged.connect(self.handleTabChange)
        self.tabs.setTabsClosable(True)
        self.tabs.tabCloseRequested.connect(lambda index: self.handleTabClose(index))
        self.tabs.setMovable(True)

        # tab visuals
        self.tabs.setTabShape(QtGui.QTabWidget.Rounded)  # .Triangular has issues on W7, fine on Ubuntu 12.04?
        self.tabs.setTabPosition(QtGui.QTabWidget.West)

        self.setWindowTitle("PyIRC")
        self.setWindowIcon(QtGui.QIcon("favicon.png"))
        self.showMaximized()

        style = self.style()
        icon = QtGui.QIcon(style.standardPixmap(QtGui.QStyle.SP_FileIcon))
        self.tray = TrayIcon(icon, self)
        self.tray.show()

        self.setCentralWidget(self.tabs)

    def handleTabChange(self):
        self.tabs.currentWidget().entry.setFocus()

    def handleTabClose(self, index):
        """
        A tab is either a server tab, a channel tab, or a private message tab with some user.

        Upon closing each kind:
        Server tab: write QUIT
        Channel tab: write PART
        PM tab: don't write anything
        """
        server, channel = "", ""
        name = [tab for tab in self.tabID if self.tabID[tab] == self.tabs.widget(index)][0]
        #print("THE NAME IS", name)
        if ":" in name:
            server, channel = name.split(":")
        else:
            server = self.tabs.tabText(index)
        if channel and channel[0] in {'&', '#', '+', '!'}:  # channel tab
            # find the server the channel is a part of and send the message to the appropriate socket
            self.sockets[self.tabID[server + ":" + channel]].write("PART %s\r\n" % channel)
        elif channel:  # PM tab
            pass
        else:  # server tab
            self.sockets[self.tabID[server]].write("QUIT : \r\n")
            self.sockets[self.tabID[server]].flush()
            self.sockets[self.tabID[server]].disconnectFromHost()
            """for tab in self.tabID:
                if tab.startswith(server + ":"):
                    self.tabs.removeTab(self.tabs.indexOf(self.tabID[tab]))
                    del self.sockets[self.tabID[tab]]
                    del self.tabID[tab]
            """
            tabs = [tab for tab in self.tabID if tab.startswith(server + ":")]
            for tab in tabs:
                self.tabs.removeTab(self.tabs.indexOf(self.tabID[tab]))
                del self.sockets[self.tabID[tab]]
                del self.tabID[tab]
        # since there may have been tab removals in the case of a server tab, need to get the index again
        self.tabs.removeTab(self.tabs.indexOf(self.tabID[name]))
        del self.sockets[self.tabID[name]]
        del self.tabID[name]

    def handleEntry(self):
        tab = self.tabs.currentWidget()
        entry = tab.entry.text()
        if entry == "":
            pass
        elif entry.startswith("/join"):
            self.joinChannel(entry.split(" ")[1])  # TODO have to grab what the JOIN message says; sometimes room names are capitalized but the server will accept lowercase version
            tab.entry.clear()
        elif entry.startswith("/pm"):
            self.privateMessage(entry.split(" ")[1])  # TODO user might not even exist
            tab.entry.clear()
        else:
            channel = ""
            current = [tab for tab in self.tabID if self.tabID[tab] == self.tabs.currentWidget()][0]
            if ":" in current:
                channel = current.split(":")[1]
            if channel:
                self.sockets[self.tabs.currentWidget()].write("PRIVMSG %s :%s\r\n" % (channel, entry))
                tab.text.append(self.time.toString("H:m") + " <" + "khyryk" + "> " + entry)
            tab.entry.clear()
            scrollBar = tab.text.verticalScrollBar()
            scrollBar.setValue(scrollBar.maximum())

    def addTab(self, server, channel=""):
        tab = QtGui.QWidget()
        grid = QtGui.QGridLayout()

        tab.text = QtGui.QTextBrowser(tab)
        tab.text.setOpenExternalLinks(True)
        tab.text.setStyleSheet(
            "background: transparent;\
            color: white;"
        )
        tab.text.setFrameStyle(QtGui.QFrame.NoFrame)
        tab.text.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        grid.addWidget(tab.text, 0, 0, 1, 1)

        tab.entry = QtGui.QLineEdit(tab)
        tab.entry.returnPressed.connect(self.handleEntry)
        grid.addWidget(tab.entry, 1, 0, 1, 1)
        tab.entry.setFrame(False)  # hides frame; not possible to enable without adding white background
        tab.entry.setPlaceholderText(">")
        tab.entry.setStyleSheet(
            "background: transparent;\
            color: white;"
        )
        # TESTING
        tab.text.setAcceptRichText(False)

        tab.setLayout(grid)
        if channel != "":
            self.tabs.addTab(tab, channel)
            self.tabID[server + ":" + channel] = tab
            self.sockets[tab] = self.sockets[self.tabID[server]]
        else:
            self.tabs.addTab(tab, server)
            self.tabID[server] = tab
        return tab

    def connectToServer(self, server, port):
        socket = QtNetwork.QTcpSocket()
        socket.connectToHost(server, port)
        socket.connected.connect(lambda: self.setUpConnection(server, socket))

    def setUpConnection(self, server, socket):
        tab = self.addTab(server)
        self.sockets[tab] = socket
        self.sockets[tab].readyRead.connect(lambda: self.handleSocketRead(server))
        nick, ident, server, name = "khyryk", "kh", server, "kh"
        self.sockets[tab].write(("USER %s %s %s :%s\r\n" % (nick, ident, server, name)).encode())
        self.sockets[tab].write(("NICK %s\r\n" % nick).encode())

    def connectPrompt(self):
        text, ok = QtGui.QInputDialog.getText(None, "Connect", "Server:")
        if text and ok:
            self.connectToServer(text, 6667)

    def joinChannel(self, channel):
        socket = self.sockets[self.tabs.currentWidget()]
        current = [tab for tab in self.tabID if self.tabID[tab] == self.tabs.currentWidget()][0]  # UGLY...
        server = ""
        if ":" in current:
            server = current.split(":")[0]
        else:
            server = current
        socket.write(("JOIN %s\r\n" % channel).encode())
        socket.flush()
        tab = self.addTab(server, channel)  # TODO have to grab what the JOIN message says; sometimes room names are capitalized but the server will accept lowercase version

    def privateMessage(self, user):
        socket = self.sockets[self.tabs.currentWidget()]
        current = [tab for tab in self.tabID if self.tabID[tab] == self.tabs.currentWidget()][0]  # UGLY...
        server = ""
        if ":" in current:
            server = current.split(":")[0]
        else:
            server = current
        tab = self.addTab(server, user)  # TODO have to grab what the JOIN message says; sometimes room names are capitalized but the server will accept lowercase version

    @staticmethod
    def parseMsg(msg):
        """Breaks a message from an IRC server into its prefix, command, and arguments.
        """
        prefix = ''
        trailing = []
        if not msg:
            pass
        if msg[0] == ':':
            prefix, msg = msg[1:].split(' ', 1)
        if msg.find(' :') != -1:
            msg, trailing = msg.split(' :', 1)
            args = msg.split()
            args.append(trailing)
        else:
            args = msg.split()
        command = args.pop(0)
        return prefix, command, args

    def handleSocketRead(self, server):
        socket = self.sockets[self.tabID[server]]
        while socket.canReadLine():  # there can be more than 1 line to read
            received = socket.readLine().__str__().rstrip()
            print(received)
            if "PING" in received:
                daemon = received.split(" ")[1]
                socket.write(("PONG %s\r\n" % daemon).encode())
            else:
                prefix, command, args = self.parseMsg(received)
                if command == "PRIVMSG":
                    user = prefix.split("!")[0]
                    if args[0] == "khyryk":  # this is a private message
                        if server + ":" + user not in self.tabID:  # if tab for this doesn't exit, make it first
                            tab = self.addTab(server, user)
                            #self.tabID[server + ":" + user] = tab
                            #font = QtGui.QFont.setBold(QtGui.QFont.Bold)
                            #tab.setFont(font)
                            #tab.setStyleSheet("QTabBar::tab:selected { font: bold; color: green; }")
                        self.tabID[server + ":" + user].text.append(self.time.toString("H:m") + " <" + user + "> " + args[1])
                    else:
                        self.tabID[server + ":" + args[0]].text.append(self.time.toString("H:m") + " <" + user + "> " + args[1])
                else:
                    self.tabID[server].text.append(received)
