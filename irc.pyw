import sys
from PySide import QtGui, QtCore, QtNetwork
import gui

"""
todo:

establish tab -> socket mapping
    * could try to do it with a tab heirarchy of server: {channel, channel, ..., channel}
    * upon a /quit, find out which socket was closed and notify all tabs that use it that the connection is closed
        without automatically closing all tabs that use the socket
"""


def main():
    app = QtGui.QApplication(sys.argv)
    #interface = gui.IrcGui()
    gui.IrcGui()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
