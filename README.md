A Python IRC client. Uses the Qt framework via PySide.

For some time, I wanted an IRC client to look just like I wanted it to look. This project is a (mostly complete) realization of that want.

![](http://i.imgur.com/0lrj85b.jpg)